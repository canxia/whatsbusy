from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

MEMBERSHIP_FIELDS = ['PREMIUM', 'NORMAL']


class User(AbstractUser):
    # stripe id number
    stripe_id = models.CharField(name='stripe_id', max_length=50, unique=True, null=True)
    # membership status
    membership = models.CharField(name='membership', max_length=20, default='NORMAL')
    # stripe subscription id number
    subscription_id = models.CharField(name='subscription_id', max_length=50, unique=True, null=True)

    def get_membership(self):
        return self.membership

    def set_membership(self, membership):
        assert membership in MEMBERSHIP_FIELDS
        self.membership = membership

    def get_stripe_id(self):
        return self.stripe_id

    def set_stripe_id(self, stripe_id):
        self.stripe_id = stripe_id

    def get_subscription_id(self):
        return self.subscription_id

    def set_subscription_id(self, subscription_id):
        self.subscription_id = subscription_id

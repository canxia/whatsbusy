from django.contrib.auth.decorators import login_required
from django.shortcuts import render, HttpResponseRedirect
from django.urls import reverse_lazy
from django_registration import signals
from django_registration.views import RegistrationView as BaseRegistrationView
from django.contrib.auth import authenticate, get_user_model, login
from django.utils.timezone import make_aware
from django.conf import settings
from datetime import datetime
import stripe

stripe.api_key = settings.STRIPE_SECRET_KEY
membership_str_d = {'PREMIUM': 'premium', 'NORMAL': 'normal'}
User = get_user_model()


# Create your views here.

# index page, show the user's membership status
@login_required
def index_view(request):
    membership_str = membership_str_d[request.user.get_membership()]
    subscription = stripe.Subscription.retrieve(
        request.user.get_subscription_id()) if membership_str == 'premium' else None
    subscription_status = subscription['status'] if subscription else None
    charge_date = make_aware(datetime.fromtimestamp(subscription['current_period_end'])) if subscription else None
    return render(request, 'core/index.html', context={
        'membership_str': membership_str, 'subscription_status': subscription_status, 'charge_date': charge_date
    })


# cancel user's subscription
@login_required
def subcription_cancel(request):
    stripe.Subscription.delete(request.user.get_subscription_id())
    request.user.set_subscription_id('')
    request.user.set_membership('NORMAL')
    request.user.save()
    return HttpResponseRedirect('/')


# start user's subscription
@login_required
def subscription_create(request):
    subscription = stripe.Subscription.create(
        customer=request.user.get_stripe_id(),
        items=[
            {'plan': settings.PRODUCT_ID}
        ],
        trial_from_plan=False,
    )
    request.user.set_subscription_id(subscription['id'])
    request.user.set_membership('PREMIUM')
    request.user.save()
    return HttpResponseRedirect('/')


# register, the stripe customer and subscription are created automatically
class RegistrationView(BaseRegistrationView):
    """
    Registration via the simplest possible process: a user supplies a
    username, email address and password (the bare minimum for a
    useful account), and is immediately signed up and logged in.

    """

    success_url = reverse_lazy("django_registration_complete")

    def register(self, form):
        new_user = form.save()
        try:
            # create stripe customer
            stripe_customer = stripe.Customer.create(
                name=new_user.get_username(),
                email=new_user.email,
                source=settings.TEST_CARD_TOKEN,
            )
            new_user.set_stripe_id(stripe_customer['id'])
            # create stripe subscription
            subscription = stripe.Subscription.create(
                customer=stripe_customer['id'],
                items=[
                    {'plan': settings.PRODUCT_ID}
                ],
                trial_from_plan=True,
            )
            new_user.set_subscription_id(subscription['id'])
            new_user.set_membership('PREMIUM')
            new_user.save()
        except Exception as e:
            new_user.delete()
            return HttpResponseRedirect('/')
        new_user = authenticate(
            **{
                User.USERNAME_FIELD: new_user.get_username(),
                "password": form.cleaned_data["password1"],
            }
        )

        login(self.request, new_user)
        signals.user_registered.send(
            sender=self.__class__, user=new_user, request=self.request
        )
        return new_user

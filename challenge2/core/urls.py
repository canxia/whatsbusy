from django.urls import path
from .views import *

# urls for the subscription
urlpatterns = [
    path('', index_view, name='index_view'),
    path('subcription/cancel', subcription_cancel, name='subscription_cancel'),
    path('subcription/create', subscription_create, name='subscription_create'),
]
from django_registration.forms import RegistrationForm
from .models import User


# The register form for the custom user model
class UserForm(RegistrationForm):
    class Meta(RegistrationForm.Meta):
        model = User

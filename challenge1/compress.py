# Time Complexity: O(N)
# Space Complexity: O(N)
# N represents the length of the string
# The compress only iterates through the input string once


def compress(s):
    res = []
    i = 0
    n = len(s)
    while i < n:
        start = i
        while i + 1 < n and s[i] == s[i + 1]:
            i += 1
        res.append('{}{}'.format(s[start], i - start + 1))
        i += 1
    res = ''.join(res)
    return res if len(res) < len(s) else s


if __name__ == "__main__":
    assert compress('bbcceeee') == 'b2c2e4'
    assert compress('aaabbbcccaaa') == 'a3b3c3a3'
    assert compress('a') == 'a'

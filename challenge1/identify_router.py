# Time Complexity: O(N)
# Space Complexity: O(1)
# N represents the number of nodes in the graph
# The identify_router only iterates through the nodes in the graph once.

from collections import defaultdict


class Graph:
    def __init__(self):
        self.v_set = set()
        self.inbound_d = defaultdict(list)
        self.outbound_d = defaultdict(list)

    def add_edge(self, src_v, dest_v):
        self.v_set.add(src_v)
        self.v_set.add(dest_v)
        self.outbound_d[src_v].append(dest_v)
        self.inbound_d[dest_v].append(src_v)


def convert_list_to_graph(graph_l):
    graph = Graph()
    for i in range(len(graph_l) - 1):
        graph.add_edge(graph_l[i], graph_l[i + 1])
    return graph


def identify_router(graph):
    res = -1
    max_adj = -1
    for v in graph.v_set:
        connections = len(graph.inbound_d[v]) + len(graph.outbound_d[v])
        if connections > max_adj:
            max_adj = connections
            res = v
    return res


if __name__ == "__main__":
    assert identify_router(convert_list_to_graph([1, 2, 3, 5, 2, 1])) == 2
    assert identify_router(convert_list_to_graph([1, 3, 5, 6, 4, 5, 2, 6])) == 5
    assert identify_router(convert_list_to_graph([2, 4, 6, 2, 5, 6])) in [2, 6]

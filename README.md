Instructions for challenge2 folder:

1. Install all dependencies of this project: pip install -r requirements.txt
2. Change the database configuration in challenge2/settings.py
3. Initial the database: python manage.py migrate
4. Start the server: python manage.py runserver
